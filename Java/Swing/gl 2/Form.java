package com.company;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Form extends JFrame {

    public Form(){
        JLabel labelStart = new JLabel("Первая страница");
        JTextField textStart = new JTextField(10);
        JLabel labelEnd = new JLabel("Последняя страница");
        JTextField textEnd = new JTextField(10);
        JButton startButton = new JButton("Start");
        startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int start = Integer.parseInt(textStart.getText());
                int end = Integer.parseInt(textEnd.getText());
                /*parser.setParserSettings(new HabrSettings(start, end));
                try {
                    parser.Start();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }*/
            }
        });
        JButton abortButton = new JButton("Abort");

        JPanel panel = new JPanel();
        JTextArea textArea = textArea = new JTextArea(21,30);
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
        JScrollPane jsp = new JScrollPane(textArea);
        jsp.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        panel.add(jsp);


        GroupLayout layout = new GroupLayout(getContentPane());
        setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        layout.setHorizontalGroup(layout.createSequentialGroup()
                .addComponent(panel)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(labelStart)
                        .addComponent(textStart)
                        .addComponent(labelEnd)
                        .addComponent(textEnd))

                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(startButton,60,80,100)
                                .addComponent(abortButton,60,80,100)))));
        layout.setVerticalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(panel)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(labelStart)
                                .addComponent(textStart,23,23,23)
                                .addComponent(labelEnd)
                                .addComponent(textEnd,23,23,23)

                                .addGroup(layout.createParallelGroup()
                                        .addComponent(startButton)
                                        .addComponent(abortButton)))));
        setSize(600, 405);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //pack();
    }
}
