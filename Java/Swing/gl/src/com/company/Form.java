package com.company;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Form extends JFrame {

    public Form(){
        JLabel label1 = new JLabel("элемент 1");

        JLabel label2 = new JLabel("элемент 2");




        GroupLayout layout = new GroupLayout(getContentPane());
        setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        //Sequential
        //Parallel

        layout.setHorizontalGroup(layout.createSequentialGroup()
                        .addComponent(label1)
                        .addComponent(label2)
                );
        layout.setVerticalGroup(layout.createSequentialGroup()
                        .addComponent(label1)
                        .addComponent(label2)
                );

        setSize(200, 100);


        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //pack();
    }
}
